module.exports = {
    devServer:{
        proxy:{
            '/ajax':{
                target:'https://m.maoyan.com',
                pathRewrite:{'^/ajax':''},
                changeOrigin:true
            },
            '/gateway':{
                target:'https://m.maizuo.com',
                changeOrigin:true
            }
        }
    },
    productionSourceMap: false,
}