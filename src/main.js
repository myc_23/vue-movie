import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/Vant'
import FastClick from 'fastclick'

Vue.filter('setWH', (url, arg) => {
  return url.replace(/2500x2500/, arg);
})
Vue.config.productionTip = false
//解决移动端300ms延迟
FastClick.attach(document.body)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
