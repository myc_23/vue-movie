import request from './request';
// 获取首页热门上映列表
export const GetmovieOnInfoListAPI = (params) => request.get("/movieOnInfoList", { params })
// 获取首页准备上映列表
export const GetcomingListAPI = (params) => request.get("/comingList", { params })
// 获取搜索列表
export const GetserchAPI = (params) => request.get("/search", { params })
// 获取电影详情页
export const GetdetailAPI = (params) => request.get("/detailmovie", { params })
// 获取影院筛选
export const GetcinemaAPI = (params) => request.get("/filterCinemas", { params })
// 获取影院列表
export const GetmoreCinemaListAPI = (params) => request.get("/moreCinemas", { params })
// 查询电影评论
// https://m.maoyan.com/mmdb/comments/movie/248566.json?_v_=yes&offset=1
// export const GetmorecommentsAPI = (params) => request.get(`/mmdb/comments/movie/${params.id}.json?_v_=yes&offset=1`)

