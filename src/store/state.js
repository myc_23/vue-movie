const state = {
    orderList: JSON.parse(localStorage.getItem("orderList")) || [],// 订单列表
    wantSee: JSON.parse(localStorage.getItem("wantSee")) || [],// 想看列表
    cityId: localStorage.getItem("cityId") || 0,// 城市id 默认是空
    cityNm: localStorage.getItem("cityNm") || "未定位",// 城市名字 默认是北京
    Vuexgross:JSON.parse(localStorage.getItem("Vuexgross"))
}
export default state