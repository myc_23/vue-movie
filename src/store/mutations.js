import { Toast } from "vant";
const mutations = {
    ADDORDERLIST(state, val) {
        if (state.orderList == undefined || state.orderList.length == 0) {
            state.orderList.push(val)
        } else {
            for (let i = 0; i < state.orderList.length; i++) {
                const element = state.orderList[i];
                if (element.id == val.id) {
                    element.price = parseInt(element.price) + parseInt(val.price)
                    return
                }
            }
            state.orderList.push(val)
        }
        localStorage.setItem("orderList", JSON.stringify(state.orderList))
    },
    // 删除
    DELETEORDER(state, val) {
        state.orderList.splice(val, 1)
        localStorage.setItem("orderList", JSON.stringify(state.orderList))
        Toast("删除成功");
    },

    // 删除想看
    DELETEORDERSEE(state, val) {
        state.wantSee.splice(val, 1)
        localStorage.setItem("wantSee", JSON.stringify(state.wantSee))
        Toast("删除成功");
    },
    // 全部删除
    ALLDELETE(state) {
        state.orderList = [],
            state.wantSee = [],
            state.cityId = 1,
            state.cityNm = "北京",
            localStorage.setItem("orderList", JSON.stringify(state.orderList)),
            localStorage.setItem("wantSee", JSON.stringify(state.wantSee))
        var storage = window.localStorage;
        storage.clear();
        Toast.loading({
            message: "正在清理...",
            forbidClick: true,
        });
        setTimeout(() => {
            Toast("清理成功");
        }, 2000);
    },
    // 申请退款
    REFUND(state, val) {
        for (let i = 0; i < state.orderList.length; i++) {
            const element = state.orderList[i];
            if (element.id == val.id) {
                element.price = parseInt(element.price) - parseInt(val.price)
                localStorage.setItem("orderList", JSON.stringify(state.orderList))
                return
            }
        }
    },
    // 添加想看
    AADTOSEE(state, val) {
        state.wantSee.push(val)
        localStorage.setItem("wantSee", JSON.stringify(state.wantSee))
        Toast("已添加到收藏列表");
        return
    },
    // 切换城市
    CHANGECITY(state, val) {
        state.cityId = val.id,
            state.cityNm = val.nm
        localStorage.setItem("cityId", val.id)
        localStorage.setItem("cityNm", val.nm)
    },
}
export default mutations
// CHANGECITY(state,val){}
