import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import Cinema from '@/views/Cinema.vue'
import User from '@/views/User.vue'
import detail from '@/components/detail/detail_page.vue'
import buyticket from '@/components/common/buy_ticket.vue'
import order from '@/components/user/order_page.vue'
import orderlist from '@/components/user/order_page_list.vue'
import wantsee from '@/components/user/want_see.vue'
import citys from '@/components/home/citys.vue'

Vue.use(VueRouter)

const routes = [
  // 重定向
  { path: '/', redirect: '/Home' },
  { path: '/Home', name: 'VueMovieHome', component: Home },
  { path: '/Cinema', name: 'VueMovieCinema', component: Cinema },
  { path: '/User', name: 'VueMovieUser', component: User },
  { path: '/buyticket', name: 'VueMovieBuyTicket', component: buyticket },
  { path: '/detail', path: '/detail/:id', name: 'VueMovieDetailPage', component: detail },
  { path: '/order', name: 'VueMovieOrderPage', component: order },
  { path: '/orderlist', name: 'VueMovieOrderPageList', component: orderlist },
  { path: '/wantsee', name: 'VueMovieWantSee', component: wantsee },
  { path: '/citys', name: 'VueMovieCitys', component: citys },

]
const router = new VueRouter({
  routes
})

export default router
