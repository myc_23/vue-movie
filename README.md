
- 线上体验地址:http://175.178.72.198:81/#/Home

- 技术栈：Vue+Vant+axios+Vuex

- 第一步: npm install

- 第二步: npm run serve

- ps:如果安装失败 可以先安装cnpm 再用cnpm安装

- 实现功能：
- 电影的搜索
- 电影详情 购买和选座 仿 支付/退款
- 电影收藏和管理
- 订单管理 影院地址展示 
- 城市地址切换 电影网友评论 等

![输入图片说明](src/assets/%E7%A8%8B%E5%BA%8F%E6%88%AA%E5%9B%BE/1.png)
![输入图片说明](src/assets/%E7%A8%8B%E5%BA%8F%E6%88%AA%E5%9B%BE/2.png)
![输入图片说明](src/assets/%E7%A8%8B%E5%BA%8F%E6%88%AA%E5%9B%BE/3.png)
![输入图片说明](src/assets/%E7%A8%8B%E5%BA%8F%E6%88%AA%E5%9B%BE/4.png)
![输入图片说明](src/assets/%E7%A8%8B%E5%BA%8F%E6%88%AA%E5%9B%BE/5.png)
![输入图片说明](src/assets/%E7%A8%8B%E5%BA%8F%E6%88%AA%E5%9B%BE/6.png)
![输入图片说明](src/assets/%E7%A8%8B%E5%BA%8F%E6%88%AA%E5%9B%BE/7.png)
